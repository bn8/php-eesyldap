<?php

namespace EesyLDAP\Schema;


/**
 * @property-read string $oid
 * @property-read string $name
 * @property-read array<string> $applies
 */
class MatchingRuleUse extends SchemaEntry {

  /**
   * Default properties value
   * @var array<string,mixed>
   */
  protected static $default_properties = array(
    'oid' => null,
    'name' => null,
    'applies' => array(),
  );

}

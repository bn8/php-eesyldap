<?php

namespace EesyLDAP\Schema;

use EesyLDAP\Schema;

/**
 * @property-read string $oid
 * @property-read string $name
 * @property-read string|null $desc
 * @property-read string|null $description
 * @property-read bool $obselete
 * @property-read string|null $sup
 * @property-read string|null $superior
 * @property-read string|null $equality
 * @property-read string|null $ordering
 * @property-read string|null $substr
 * @property-read string|null $substring
 * @property-read string|null $syntax
 * @property-read int|null $max_length
 * @property-read bool $single
 * @property-read bool $single_value
 * @property-read bool $multiple
 * @property-read bool $collective
 * @property-read string|null $usage
 * @property-read bool $no_user_modification
 * @property-read array<string,string> $property_aliases
 * @property-read array<string,string> $map_syntax_to_subtypes
 */
class Attribute extends SchemaEntry {

  /**
   * Default properties value
   * @var array<string,mixed>
   */
  protected static $default_properties = array(
    'oid' => null,
    'name' => null,
    'desc' => null,
    'obsolete' => false,
    'sup' => null,
    'equality' => null,
    'ordering' => null,
    'substr' => null,
    'syntax' => null,
    'max_length' => null,
    'single-value' => false,
    'collective' => false,
    'usage' => null,
    'no-user-modification' => false,
  );

  /**
   * Properties name aliases
   * @var array<string,string>
   */
  protected static $property_aliases = array(
    'description' => 'desc',
    'superior' => 'sup',
    'substring' => 'substr',
    'single' => 'single-value',
    'single_value' => 'single-value',
    'no_user_modification' => 'no-user-modification',
  );

  /**
   * Computed properties name
   * @var array<string>
   */
  protected static $computed_properties = array(
    'names',
    'property_aliases',
    'map_syntax_to_subtypes',
    'multiple',
  );

  /**
   * Map attribute syntax OID to corresponding sub Attribute types
   * @var array<string,class-string<Attribute>>
   */
  protected static $map_syntax_to_subtypes = array(
    Schema::SYNTAX_BOOLEAN => Attribute\BooleanAttribute::class,
  );

  /**
   * Parse a SubSchema attribute value into a SchemaEntry object
   * @param string $value Attribute value
   * @return Attribute
   */
  public static function parse($value) {
    $schema_entry = self :: _parse($value);
    if (
      isset($schema_entry['syntax'])
      && is_string($schema_entry['syntax'])
      && array_key_exists($schema_entry['syntax'], self :: $map_syntax_to_subtypes)
    )
      $type = self :: $map_syntax_to_subtypes[$schema_entry['syntax']];
    else
      $type = self :: class;
    return new $type($schema_entry);
  }

  /**
   * Magic method to get attribute schema entry key
   * @param string $key
   * @return mixed
   * @throws \EesyLDAP\InvalidPropertyException
   */
  public function __get($key) {
    switch($key) {
      case 'multiple':
        return !$this->single;
    }
    return parent::__get($key);
  }

  /**
   * Convert LDAP values to PHP value
   * @param array<int,string> $values
   * @return string|array<string>|null
   */
  public function ldap2php($values) {
    if ($values)
      return $this->single?$values[0]:$values;
    return $this->single?null:array();
  }

  /**
   * Convert PHP value to LDAP values
   * @param mixed $value
   * @param bool $one_value Convert one value of the attribute (optional, default: false)
   * @return ( $one_value is True ? string|null : array<int,string> )
   */
  public function php2ldap($value, $one_value=false) {
    if (is_null($value))
      return $one_value?null:array();
    if ($one_value)
      return strval($value);
    if (!is_array($value))
      $value = array($value);
    $ldap_values = array();
    foreach($value as $v)
      $ldap_values[] = strval($v);
    return $ldap_values;
  }
}

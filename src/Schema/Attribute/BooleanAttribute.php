<?php

namespace EesyLDAP\Schema\Attribute;

/**
 * LDAP Schema boolean attribute
 */
class BooleanAttribute extends \EesyLDAP\Schema\Attribute {

  /**
   * LDAP stored value for true
   * @var string
   */
  const TRUE_VALUE = 'TRUE';

  /**
   * LDAP stored value for false
   * @var string
   */
  const FALSE_VALUE = 'FALSE';



  /**
   * Convert one LDAP value to PHP value
   * @param string $value
   * @return bool|null
   */
  protected function _ldap2php($value) {
    if ($value == self :: TRUE_VALUE)
      return true;
    if ($value == self :: FALSE_VALUE)
      return false;
    return null;
  }
  /**
   * Convert LDAP values to PHP value
   * @param array<int,string> $values
   * @return bool|null|array<bool|null>
   */
  public function ldap2php($values) {
    $values = parent::ldap2php($values);
    if (is_string($values))
      return self :: _ldap2php($values);
    if (is_array($values)) {
      $php_value = array();
      foreach($values as $value)
        $php_value[] = self :: _ldap2php($value);
      return $php_value;
    }
    return $this->single?null:array();
  }

  /**
   * Convert PHP value to LDAP values
   * @param mixed $value
   * @param bool $one_value Convert one value value of the attribute (optional, default: false)
   * @return ( $one_value is True ? string|null : array<int,string> )
   */
  public function php2ldap($value, $one_value=false) {
    if (is_null($value))
      return $one_value?null:array();
    if ($one_value)
      return $value?self :: TRUE_VALUE:self :: FALSE_VALUE;
    $value = is_array($value)?$value:array($value);
    $ldap_values = array();
    foreach(parent::php2ldap($value) as $value)
      $ldap_values[] = $value?self :: TRUE_VALUE:self :: FALSE_VALUE;
    return $ldap_values;
  }
}

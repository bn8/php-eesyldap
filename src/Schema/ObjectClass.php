<?php

namespace EesyLDAP\Schema;


/**
 * @property-read string $oid
 * @property-read string $name
 * @property-read string|null $desc
 * @property-read string|null $description
 * @property-read bool $obselete
 * @property-read string|null $sup
 * @property-read string|null $superior
 * @property-read bool $abstract
 * @property-read bool $structural
 * @property-read bool $auxiliary
 * @property-read string|null $type
 * @property-read array<string> $must
 * @property-read array<string> $may
 * @property-read array<string,string> $property_aliases
 */
class ObjectClass extends SchemaEntry {

  /**
   * Default properties value
   * @var array<string,mixed>
   */
  protected static $default_properties = array(
    'oid' => null,
    'name' => null,
    'desc' => null,
    'obselete' => false,
    'sup' => null,
    'abstract' => false,
    'structural' => false,
    'auxiliary' => false,
    'must' => array(),
    'may' => array(),
  );

  /**
   * Properties name aliases
   * @var array<string,string>
   */
  protected static $property_aliases = array(
    'description' => 'desc',
    'superior' => 'sup',
  );

  /**
   * Computed properties name
   * @var array<string>
   */
  protected static $computed_properties = array(
    'names',
    'property_aliases',
    'type',
  );

  /**
   * Magic method to get objectclass schema entry key
   * @param string $key
   * @return mixed
   * @throws \EesyLDAP\InvalidPropertyException
   */
  public function __get($key) {
    switch($key) {
      case 'type':
        if ($this->abstract)
          return 'abstract';
        if ($this->structural)
          return 'structural';
        if ($this->auxiliary)
          return 'auxiliary';
        return null;
    }
    return parent::__get($key);
  }

  /**
   * Check if the given attribute is used by the objectclass
   * @param string $attr The attribute name
   * @return bool
   */
  public function has_attribute($attr) {
    if (in_array($attr, $this->must))
      return true;
    if (in_array($attr, $this->may))
      return true;
    return false;
  }
}

<?php

namespace EesyLDAP\Schema;


/**
 * @property-read string|null $oid
 * @property-read string|null $desc
 * @property-read string|null $description
 */
class Syntax extends SchemaEntry {

  /**
   * Default properties value
   * @var array<string,mixed>
   */
  protected static $default_properties = array(
    'oid' => null,
    'desc' => null,
  );

  /**
   * Properties name aliases
   * @var array<string,string>
   */
  protected static $property_aliases = array(
    'description' => 'desc',
  );

}

<?php

namespace EesyLDAP\Schema;


/**
 * @property-read string $oid
 * @property-read string $name
 * @property-read string|null $syntax
 */
class MatchingRule extends SchemaEntry {

  /**
   * Default properties value
   * @var array<string,mixed>
   */
  protected static $default_properties = array(
    'oid' => null,
    'name' => null,
    'syntax' => null,
  );

}

<?php
namespace EesyLDAP;


class Link {

  /**
   * The LDAP connection
   * @var resource|false
   */
  protected $_link = false;

  /**
   * Connect on LDAP host
   * @param string $uri LDAP server URI
   * @param int|null $port Optional LDAP server port (default: null == 389)
   * @see ldap_connect()
   * @return bool
   */
  public function connect($uri, $port=null) {
    $this->_link = @ldap_connect($uri, is_null($port)?389:$port);
    return boolval($this->_link);
  }

  /**
   * Start TLS
   * @see ldap_start_tls()
   * @return bool
   */
  public function start_tls() {
    if (!$this -> _link) return false;
    return @ldap_start_tls($this->_link);
  }

  /**
   * Return the LDAP error number of the last LDAP command
   * @see ldap_errno()
   * @return int|false
   */
  public function errno() {
    if (!$this -> _link) return false;
    return @ldap_errno($this->_link);
  }

  /**
   * Convert LDAP error number into string error message
   * @param int $errno
   * @see ldap_err2str()
   * @return string
   */
  public function err2str($errno) {
    return @ldap_err2str($errno);
  }

  /**
   * Bind on LDAP link
   * @param string|null $dn Bind DN (optional, default: null = anonymous bind)
   * @param string|null $password Bind password (optional, default: null = anonymous bind)
   * @see ldap_bind()
   * @return bool
   */
  public function bind($dn=null, $password=null) {
    if (!$this -> _link) return false;
    return @ldap_bind($this->_link, $dn, $password);
  }

  /**
   * Set an LDAP option
   *
   * @param int $option Option to set
   * @param string|int|bool $value  Value to set Option to
   *
   * @see ldap_set_option()
   * @access public
   * @return bool
   */
  public function set_option($option, $value) {
    if (!$this->_link)
      return false;
    return @ldap_set_option($this->_link, $option, $value);
  }

  /**
   * Single-level search
   *
   * @param string $base The base DN of the search
   * @param string $filter The LDAP filter string of the search
   * @param array<string> $attributes Array of expected attribute names (optional, default: all)
   * @param int $attributes_only If 1, only attribute names will be return (optional, default: 0)
   * @param int $sizelimit Limit the count of entries fetched (optional, default: -1)
   * @param int $timelimit Number of seconds how long is spend on the search (optional, default: -1)
   * @param int $deref Specifies how aliases should be handled during the search (optional,
   *                   default: LDAP_DEREF_NEVER)
   * @param array<array<mixed>> $controls Array of LDAP Controls to send with the request
   *                            (optional, default: null)
   *
   * @see ldap_list()
   * @access public
   * @return resource|false
   */
  public function list(
    $base, $filter, $attributes=[], $attributes_only=0, $sizelimit=-1, $timelimit=-1,
    $deref=LDAP_DEREF_NEVER, $controls=[]
  ) {
    if (!$this->_link)
      return false;
    return @ldap_list(
      $this->_link, $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit, $deref,
      $controls
    );
  }

  /**
   * Read an LDAP entry
   *
   * @param string $base The base DN of the search
   * @param string $filter The LDAP filter string of the search
   * @param array<string> $attributes Array of expected attribute names (optional, default: all)
   * @param int $attributes_only If 1, only attribute names will be return (optional, default: 0)
   * @param int $sizelimit Limit the count of entries fetched (optional, default: -1)
   * @param int $timelimit Number of seconds how long is spend on the search (optional, default: -1)
   * @param int $deref Specifies how aliases should be handled during the search (optional,
   *                   default: LDAP_DEREF_NEVER)
   * @param array<array<mixed>> $controls Array of LDAP Controls to send with the request
   *                            (optional, default: empty array)
   *
   * @see ldap_read()
   * @access public
   * @return resource|false
   */
  public function read(
    $base, $filter, $attributes=[], $attributes_only=0, $sizelimit=-1, $timelimit=-1,
    $deref=LDAP_DEREF_NEVER, $controls=[]
  ) {
    if (!$this->_link)
      return false;
    return ldap_read(
      $this->_link, $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit, $deref,
      $controls
    );
  }

  /**
   * Search in LDAP tree
   *
   * @param string $base The base DN of the search
   * @param string $filter The LDAP filter string of the search
   * @param array<string> $attributes Array of expected attribute names (optional, default: all)
   * @param int $attributes_only If 1, only attribute names will be return (optional, default: 0)
   * @param int $sizelimit Limit the count of entries fetched (optional, default: -1)
   * @param int $timelimit Number of seconds how long is spend on the search (optional, default: -1)
   * @param int $deref Specifies how aliases should be handled during the search (optional,
   *                   default: LDAP_DEREF_NEVER)
   * @param array<array<mixed>> $controls Array of LDAP Controls to send with the request
   *                            (optional, default: empty array)
   *
   * @see ldap_search()
   * @access public
   * @return resource|false
   */
  public function search(
    $base, $filter, $attributes=[], $attributes_only=0, $sizelimit=-1, $timelimit=-1,
    $deref=LDAP_DEREF_NEVER, $controls=[]
  ) {
    if (!$this->_link)
      return false;
    return @ldap_search(
      $this->_link, $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit, $deref,
      $controls
    );
  }

  /**
   * Get all result entries
   * @param resource $result An LDAP\Result instance, returned by ldap_list() or ldap_search()
   * @see ldap_get_entries()
   * @return array<mixed>|false
   */
  public function get_entries($result) {
    if (!$this -> _link) return false;
    return @ldap_get_entries($this->_link, $result);
  }

  /**
   * Close LDAP link (if established)
   * @see ldap_close()
   * @return void
   */
  public function close() {
    if ($this->_link)
      @ldap_close($this->_link);
    $this -> _link = false;
  }

}

<?php

namespace EesyLDAP;


class Util {
  /**
   * Convert provided value as an array of string
   * @param mixed $values
   * @return array<int,string>
   */
  public static function ensure_array_of_string($values) {
    $result = array();
    if (!is_null($values)) {
      $values = is_array($values)?$values:array($values);
      foreach ($values as $value)
        $result[] = strval($value);
    }
    return $result;
  }
}

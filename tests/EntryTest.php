<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use EesyLDAP\Entry;
use EesyLDAP\LdapException;

/**
 * @covers \EesyLDAP\Entry
 */
final class EntryTest extends TestCase {

  /**
   * @covers \EesyLDAP\Entry::__get
   */
  public function testGetDn() {
    $dn = 'cn=admin';
    $entry = new Entry($dn);
    $this->assertEquals($dn, $entry->dn);
  }

  /**
   * @covers \EesyLDAP\Entry::__get
   */
  public function testGetChanges() {
    $entry = new Entry();
    $attr = 'cn';
    $value = 'test';
    $expected_changes = array(
      'add' => array($attr => array($value)),
      'replace' => array(),
      'delete' => array(),
    );
    $entry->add($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::__get
   */
  public function testGetAttribute() {
    $attr = 'cn';
    $values = array('test');
    $entry = new Entry(null, array($attr => $values));
    $this->assertEquals($values, $entry->__get($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::__set
   */
  public function testSetDn() {
    $entry = new Entry();
    $dn = 'cn=admin';
    $entry->dn = $dn;
    $this->assertEquals($dn, $entry->dn);
  }

  /**
   * @covers \EesyLDAP\Entry::__set
   */
  public function testSetDnAsFalse() {
    $dn = 'cn=admin';
    $entry = new Entry($dn);
    $entry->dn = false;
    $this->assertEquals(false, $entry->dn);
  }

  /**
   * @covers \EesyLDAP\Entry::__set
   */
  public function testSetDnWithBadValue() {
    $entry = new Entry();
    $this->expectException(LdapException::class);
    // @phpstan-ignore-next-line
    $entry->dn = array();
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testDnIsSet() {
    $entry = new Entry();
    $this->assertTrue(isset($entry->dn));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testChangesIsSet() {
    $entry = new Entry();
    $this->assertTrue(isset($entry->changes));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsSetWithMagicMethod() {
    $attr = 'cn';
    $entry = new Entry(null, array($attr => array('value')));
    $this->assertTrue($entry->__isset($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsNotSetWithMagicMethod() {
    $entry = new Entry();
    $this->assertFalse(isset($entry->undefined));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsSet() {
    $attr = 'cn';
    $entry = new Entry(null, array($attr => array('value')));
    $this->assertTrue($entry->attribute_is_set($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsNotSet() {
    $entry = new Entry();
    $this->assertFalse($entry->attribute_is_set('undefined'));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsSetAfterDelete() {
    $attr = 'cn';
    $value = 'test';
    $entry = new Entry(null, array($attr => array($value)));
    $entry -> delete($attr, $value);
    $this->assertFalse($entry->attribute_is_set($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsSetAfterDeleteOnlyOneOfValue() {
    $attr = 'cn';
    $values = array('test1', 'test2');
    $entry = new Entry(null, array($attr => $values));
    $entry -> delete($attr, $values[0]);
    $this->assertTrue($entry->attribute_is_set($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsSetAfterReplace() {
    $attr = 'cn';
    $original_value = 'test1';
    $value = 'test2';
    $entry = new Entry(null, array($attr => array($original_value)));
    $entry -> replace($attr, $value);
    $this->assertTrue($entry->attribute_is_set($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsSetAfterDeleteAndReplace() {
    $attr = 'cn';
    $original_value = 'test1';
    $value = 'test2';
    $entry = new Entry(null, array($attr => array($original_value)));
    $entry -> delete($attr, $original_value);
    $entry -> replace($attr, $value);
    $this->assertTrue($entry->attribute_is_set($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::__isset
   */
  public function testAttributeIsSetAfterAdd() {
    $attr = 'cn';
    $value = 'test';
    $entry = new Entry();
    $entry -> add($attr, $value);
    $this->assertTrue($entry->attribute_is_set($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddValueOnNewEntry() {
    $entry = new Entry();
    $attr = 'CN';
    $value = 'test';
    $expected_changes = array(
      'add' => array(strtolower($attr) => array($value)),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> add($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddValuesOnNewEntry() {
    $entry = new Entry();
    $attr = 'cn';
    $values = array('test1', 'test2');
    $expected_changes = array(
      'add' => array($attr => $values),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> add($attr, $values);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddNull() {
    $entry = new Entry();
    $this->expectException(LdapException::class);
    $entry -> add('cn', null);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddExistingValuesOnNewEntry() {
    $attr = 'cn';
    $values = array('test1', 'test2');
    $entry = new Entry(null, array($attr => $values));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> add($attr, $values);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddValueMultipleTimeOnNewEntry() {
    $entry = new Entry();
    $attr = 'CN';
    $value = 'test';
    $expected_changes = array(
      'add' => array(strtolower($attr) => array($value)),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> add($attr, $value);
    $entry -> add($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddValueAfterReplaceOnNewEntry() {
    $entry = new Entry();
    $attr = 'cn';
    $replace_value = 'test1';
    $value = 'test2';
    $expected_changes = array(
      'add' => array($attr => array($replace_value, $value)),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> replace($attr, $replace_value);
    $entry -> add($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddExistingValueAfterReplace() {
    $attr = 'cn';
    $replace_value = 'test1';
    $add_value = 'test2';
    $entry = new Entry(null, array($attr => array('original')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => array($replace_value, $add_value)),
      'delete' => array(),
    );
    $entry -> replace($attr, $replace_value);
    $entry -> add($attr, $add_value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddExistingValueAfterReplaceWithSameValue() {
    $attr = 'cn';
    $value = 'test';
    $entry = new Entry(null, array($attr => array('original')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => array($value)),
      'delete' => array(),
    );
    $entry -> replace($attr, $value);
    $entry -> add($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddValueAfterDeleteOnNewEntry() {
    $attr = 'cn';
    $value = 'test';
    $entry = new Entry(null, array($attr => array($value)));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> delete($attr, $value);
    $entry -> add($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::add
   */
  public function testAddValueOnBadAttribute() {
    $attr = 'cn';
    $values = array('test1', 'test2');
    $entry = $this->getMockBuilder(Entry::class)
                  ->disableOriginalConstructor()
                  ->onlyMethods(['has_attribute'])
                  ->getMock();
    $entry->expects($this->once())
          ->method('has_attribute')
          ->with($attr)
          ->willReturn(false);
    $this->expectException(LdapException::class);
    $entry->add($attr, $values);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteValue() {
    $attr = 'CN';
    $value = 'test';
    $entry = new Entry(null, array($attr => array($value)));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(strtolower($attr) => array($value)),
    );
    $entry -> delete($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteUndefinedValue() {
    $attr = 'cn';
    $value = 'test1';
    $undefined_value = 'test2';
    $entry = new Entry(null, array($attr => array($value)));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> delete($attr, $undefined_value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteValues() {
    $attr = 'CN';
    $values = array('test1', 'test2');
    $entry = new Entry(null, array($attr => $values));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(strtolower($attr) => $values),
    );
    $entry -> delete($attr, $values);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteAttribute() {
    $attr = 'CN';
    $values = array('test1', 'test2');
    $entry = new Entry(null, array($attr => $values));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(strtolower($attr) => $values),
    );
    $entry -> delete($attr);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteAttributeAfterReplace() {
    $attr = 'CN';
    $values = array('test1', 'test2');
    $entry = new Entry(null, array($attr => $values));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(strtolower($attr) => $values),
    );
    $entry -> replace($attr, 'test3');
    $entry -> delete($attr);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteUniqueValueAfterReplace() {
    $attr = 'CN';
    $values = array('test1', 'test2');
    $replace_value = 'test3';
    $entry = new Entry(null, array($attr => $values));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(strtolower($attr) => $values),
    );
    $entry -> replace($attr, $replace_value);
    $entry -> delete($attr, $replace_value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteAttributeAfterAdd() {
    $attr = 'CN';
    $values = array('test1', 'test2');
    $entry = new Entry(null, array($attr => $values));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(strtolower($attr) => $values),
    );
    $entry -> add($attr, 'test3');
    $entry -> delete($attr);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteAttributeAfterDelete() {
    $attr = 'CN';
    $values = array('test1', 'test2');
    $entry = new Entry(null, array($attr => $values));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(strtolower($attr) => $values),
    );
    $entry -> delete($attr, $values[0]);
    $entry -> delete($attr);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteUnsetAttribute() {
    $attr = 'CN';
    $values = array('test1', 'test2');
    $entry = new Entry();
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> delete($attr);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteValueAfterReplace() {
    $attr = 'cn';
    $replace_values = array('test1', 'test2');
    $value = end($replace_values);
    $entry = new Entry(null, array($attr => array('test3')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => array_slice($replace_values, 0, -1)),
      'delete' => array(),
    );
    $entry -> replace($attr, $replace_values);
    $entry -> delete($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteUnsetValueAfterReplace() {
    $attr = 'cn';
    $replace_values = array('test1', 'test2');
    $value = 'test3';
    $entry = new Entry(null, array($attr => array('test4')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => $replace_values),
      'delete' => array(),
    );
    $entry -> replace($attr, $replace_values);
    $entry -> delete($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteValueOnBadAttribute() {
    $attr = 'cn';
    $entry = $this->getMockBuilder(Entry::class)
                  ->disableOriginalConstructor()
                  ->onlyMethods(['has_attribute'])
                  ->getMock();
    $entry->expects($this->once())
          ->method('has_attribute')
          ->with($attr)
          ->willReturn(false);
    $this->expectException(LdapException::class);
    $entry->delete($attr);
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteBadPhpValue() {
    $attr = 'cn';
    $value = 'test';
    $entry = $this->getMockBuilder(Entry::class)
                  ->disableOriginalConstructor()
                  ->onlyMethods(['php2ldap'])
                  ->getMock();
    $entry->expects($this->once())
          ->method('php2ldap')
          ->with($attr, $value)
          ->willReturn(false);
    $this->assertFalse($entry->delete($attr, $value));
  }

  /**
   * @covers \EesyLDAP\Entry::delete
   */
  public function testDeleteAlreadyDeletedValue() {
    $attr = 'cn';
    $value = 'test';
    $entry = new Entry(null, array($attr => array($value)));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array($attr => array($value)),
    );
    $entry -> delete($attr, $value);
    $entry -> delete($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   */
  public function testReplaceValue() {
    $attr = 'cn';
    $value = 'test1';
    $entry = new Entry(null, array($attr => array('test2')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => array($value)),
      'delete' => array(),
    );
    $entry -> replace($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   */
  public function testReplaceUndefinedAttributeValue() {
    $attr = 'cn';
    $value = 'test1';
    $entry = new Entry();
    $expected_changes = array(
      'add' => array($attr => array($value)),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> replace($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   * @covers EesyLDAP\Util::ensure_array_of_string
   */
  public function testReplaceRawValue() {
    $attr = 'cn';
    $value = 'test1';
    $entry = new Entry(null, array($attr => array('test2')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => array($value)),
      'delete' => array(),
    );
    $entry -> replace($attr, $value, true);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   */
  public function testReplaceValueWithNull() {
    $attr = 'cn';
    $values = array('test');
    $entry = new Entry(null, array($attr => $values));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array($attr => $values),
    );
    $entry -> replace($attr, null);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   */
  public function testReplaceAfterAdd() {
    $attr = 'cn';
    $add_value = 'test1';
    $value = 'test2';
    $entry = new Entry(null, array($attr => array('test3')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => array($value)),
      'delete' => array(),
    );
    $entry -> add($attr, $add_value);
    $entry -> replace($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   */
  public function testReplaceAfterDelete() {
    $attr = 'cn';
    $value = 'test';
    $entry = new Entry(null, array($attr => array('original value')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => array($value)),
      'delete' => array(),
    );
    $entry -> delete($attr);
    $entry -> replace($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   */
  public function testReplaceAfterReplace() {
    $attr = 'cn';
    $value1 = 'test1';
    $value2 = 'test2';
    $entry = new Entry(null, array($attr => array('original value')));
    $expected_changes = array(
      'add' => array(),
      'replace' => array($attr => array($value2)),
      'delete' => array(),
    );
    $entry -> replace($attr, $value1);
    $entry -> replace($attr, $value2);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   */
  public function testReplaceWithSameValue() {
    $attr = 'cn';
    $value = 'test1';
    $entry = new Entry(null, array($attr => array($value)));
    $expected_changes = array(
      'add' => array(),
      'replace' => array(),
      'delete' => array(),
    );
    $entry -> replace($attr, $value);
    $this->assertEquals($expected_changes, $entry->changes);
  }

  /**
   * @covers \EesyLDAP\Entry::replace
   */
  public function testReplaceValueOnBadAttribute() {
    $attr = 'cn';
    $value = 'test';
    $entry = $this->getMockBuilder(Entry::class)
                  ->disableOriginalConstructor()
                  ->onlyMethods(['has_attribute'])
                  ->getMock();
    $entry->expects($this->once())
          ->method('has_attribute')
          ->with($attr)
          ->willReturn(false);
    $this->expectException(LdapException::class);
    $entry->replace($attr, $value);
  }

  /**
   * @covers \EesyLDAP\Entry::get_raw_values
   */
  public function testGetRawValues() {
    $attr = 'cn';
    $values = array('test1');
    $entry = new Entry(null, array($attr => $values));
    $this->assertEquals($values, $entry->get_raw_values($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::get_raw_values
   */
  public function testGetRawValuesOnUndefinedAttribute() {
    $entry = new Entry();
    $this->assertEquals(array(), $entry->get_raw_values('undefined'));
  }

  /**
   * @covers \EesyLDAP\Entry::get_raw_values
   */
  public function testGetRawValuesAfterAdd() {
    $attr = 'cn';
    $values = array('test1');
    $add_value = 'test2';
    $entry = new Entry(null, array($attr => $values));
    $entry->add($attr, $add_value);
    $this->assertEquals(array_merge($values, array($add_value)), $entry->get_raw_values($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::get_raw_values
   */
  public function testGetRawValuesAfterReplace() {
    $attr = 'cn';
    $values = array('test1');
    $replace_value = 'test2';
    $entry = new Entry(null, array($attr => $values));
    $entry->replace($attr, $replace_value);
    $this->assertEquals(array($replace_value), $entry->get_raw_values($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::get_raw_values
   */
  public function testGetRawValuesAfterDelete() {
    $attr = 'cn';
    $values = array('test1');
    $entry = new Entry(null, array($attr => $values));
    $entry->delete($attr);
    $this->assertEquals(array(), $entry->get_raw_values($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::get_raw_values
   */
  public function testGetRawValuesWithDefault() {
    $default = 'default';
    $entry = new Entry();
    $this->assertEquals($default, $entry->get_raw_values('undefined', $default));
  }

  /**
   * @covers \EesyLDAP\Entry::get_raw_values
   */
  public function testGetRawValuesOnlyFirst() {
    $attr = 'cn';
    $values = array('test1', 'test2');
    $entry = new Entry(null, array($attr => $values));
    $this->assertEquals($values[0], $entry->get_raw_values($attr, null, false));
  }

  /**
   * @covers \EesyLDAP\Entry::set_raw_values
   */
  public function testSetRawValues() {
    $attr = 'cn';
    $values = array('test1', 'test2');
    $entry = $this->getMockBuilder(Entry::class)
                  ->disableOriginalConstructor()
                  ->onlyMethods(['replace'])
                  ->getMock();
    $entry->expects($this->once())
          ->method('replace')
          ->with($attr, $values, true)
          ->willReturn(true);
    $this->assertTrue($entry->set_raw_values($attr, $values));
  }

  /**
   * @covers \EesyLDAP\Entry::set_value
   */
  public function testSetValue() {
    $attr = 'cn';
    $values = array('test1', 'test2');
    $entry = $this->getMockBuilder(Entry::class)
                  ->disableOriginalConstructor()
                  ->onlyMethods(['replace'])
                  ->getMock();
    $entry->expects($this->once())
          ->method('replace')
          ->with($attr, $values)
          ->willReturn(true);
    $this->assertTrue($entry->set_value($attr, $values));
  }

  /**
   * @covers \EesyLDAP\Entry::__toString
   */
  public function testToString() {
    $entry = new Entry();
    $this->assertEquals('<New LDAP Entry>', $entry->__toString());
  }

  /**
   * @covers \EesyLDAP\Entry::__toString
   */
  public function testToStringWithDN() {
    $dn = 'cn=test';
    $entry = new Entry($dn);
    $this->assertEquals("<LDAP Entry \"$dn\">", $entry->__toString());
  }

  /**
   * @covers \EesyLDAP\Entry::has_attribute
   */
  public function testHasAttribute() {
    $attr = 'test';
    $ocs = array('oc1', 'oc2');
    $schema = Mockery::mock('EesyLDAP\Schema');
    $schema->shouldReceive('has_attribute')
      ->once()
      ->with($attr, $ocs)
      ->andReturn(true);

    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('schema'))
         ->will($this->returnValue($schema));

    $entry = Mockery::mock('EesyLDAP\Entry');
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('get_raw_values')
      ->once()
      ->with('objectclass')
      ->andReturn($ocs);
    $entry->shouldReceive('has_attribute')->passthru();
    $this->assertTrue($entry->has_attribute($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::has_attribute
   */
  public function testHasAttributeNoObjectclass() {
    $attr = 'test';
    $schema = Mockery::mock('EesyLDAP\Schema');
    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('schema'))
         ->will($this->returnValue($schema));

    $entry = Mockery::mock('EesyLDAP\Entry');
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('get_raw_values')
      ->once()
      ->with('objectclass')
      ->andReturn(array());
    $entry->shouldReceive('has_attribute')->passthru();
    $this->assertFalse($entry->has_attribute($attr));
  }

  /**
   * @covers \EesyLDAP\Entry::get_value
   * @
   */
  public function testGetValueNoLdap() {
    $attr_name = 'test';
    $value = 'TRUE';
    $default = false;
    $first = true;
    $entry = $this->getMockBuilder(Entry::class)
                  ->disableOriginalConstructor()
                  ->onlyMethods(['get_raw_values'])
                  ->getMock();
    $entry->expects($this->once())
          ->method('get_raw_values')
          // @phpstan-ignore-next-line
          ->with($this->equalTo($attr_name), $this->equalTo($default), $this->equalTo(!$first))
          ->will($this->returnValue(true));
    $this->assertTrue($entry->get_value($attr_name, $default, $first));
  }

  /**
   * @covers \EesyLDAP\Entry::get_value
   */
  public function testGetValueWithSchema() {
    $attr_name = 'test';
    $value = 'TRUE';
    $attr = $this->createMock(\EesyLDAP\Schema\Attribute\BooleanAttribute::class);
    $attr->expects($this->once())
         ->method('ldap2php')
         ->with($this->equalTo(array($value)))
         ->will($this->returnValue(true));
    $schema = Mockery::mock('EesyLDAP\Schema');
    $schema->shouldReceive('attribute')
      ->once()
      ->with($attr_name)
      ->andReturn($attr);

    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('schema'))
         ->will($this->returnValue($schema));

    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('_get_raw_values')
      ->once()
      ->with($attr_name)
      ->andReturn(array($value));
    $entry->shouldReceive('get_value')->passthru();

    $this->assertTrue($entry->get_value($attr_name));
  }

  /**
   * @covers \EesyLDAP\Entry::get_value
   */
  public function testGetValueWithSchemaNoValue() {
    $attr_name = 'test';
    $attr = $this->createMock(\EesyLDAP\Schema\Attribute\BooleanAttribute::class);
    $attr->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('single'))
         ->will($this->returnValue(true));
    $schema = Mockery::mock('EesyLDAP\Schema');
    $schema->shouldReceive('attribute')
      ->once()
      ->with($attr_name)
      ->andReturn($attr);

    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('schema'))
         ->will($this->returnValue($schema));

    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('_get_raw_values')
      ->once()
      ->with($attr_name)
      ->andReturn(array());
    $entry->shouldReceive('get_value')->passthru();

    $this->assertNull($entry->get_value($attr_name));
  }

  /**
   * @covers \EesyLDAP\Entry::get_value
   */
  public function testGetValueWithSchemaNoValueMultiple() {
    $attr_name = 'test';
    $attr = $this->createMock(\EesyLDAP\Schema\Attribute::class);
    $attr->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('single'))
         ->will($this->returnValue(false));
    $schema = Mockery::mock('EesyLDAP\Schema');
    $schema->shouldReceive('attribute')
      ->once()
      ->with($attr_name)
      ->andReturn($attr);

    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('schema'))
         ->will($this->returnValue($schema));

    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('_get_raw_values')
      ->once()
      ->with($attr_name)
      ->andReturn(array());
    $entry->shouldReceive('get_value')->passthru();

    $this->assertEquals(array(), $entry->get_value($attr_name));
  }

  /**
   * @covers \EesyLDAP\Entry::get_value
   */
  public function testGetValueWithSchemaUndefinedAttribute() {
    $attr_name = 'test';
    $schema = Mockery::mock('EesyLDAP\Schema');
    $schema->shouldReceive('attribute')
      ->once()
      ->with($attr_name)
      ->andReturn(false);

    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('schema'))
         ->will($this->returnValue($schema));

    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('_get_raw_values')
      ->once()
      ->with($attr_name)
      ->andReturn(array());
    $entry->shouldReceive('error')
      ->once()
      ->with('Unkown attribute %s in schema', null, $attr_name)
      ->andThrow(LdapException::class);
    $entry->shouldReceive('get_value')->passthru();

    $this->expectException(LdapException::class);
    $entry->get_value($attr_name);
  }

  /**
   * @covers \EesyLDAP\Entry::php2ldap
   */
  public function testPhp2LdapNoLdap() {
    $attr_name = 'test';
    $values = array(1, 2);
    $entry = Mockery::mock('EesyLDAP\Entry');
    $entry->shouldReceive('php2ldap')->passthru();
    $this->assertEquals(array_map('strval', $values), $entry->php2ldap($attr_name, $values));
  }

  /**
   * @covers \EesyLDAP\Entry::php2ldap
   */
  public function testPhp2LdapNoLdapNoValue() {
    $attr_name = 'test';
    $entry = Mockery::mock('EesyLDAP\Entry');
    $entry->shouldReceive('php2ldap')->passthru();
    $this->assertEquals(array(), $entry->php2ldap($attr_name, null));
  }

  /**
   * @covers \EesyLDAP\Entry::php2ldap
   */
  public function testPhp2LdapNoLdapNotArray() {
    $attr_name = 'test';
    $value = 'test';
    $entry = Mockery::mock('EesyLDAP\Entry');
    $entry->shouldReceive('php2ldap')->passthru();
    $this->assertEquals(array($value), $entry->php2ldap($attr_name, $value));
  }

  /**
   * @covers \EesyLDAP\Entry::php2ldap
   */
  public function testPhp2LdapNoLdapOneValue() {
    $attr_name = 'test';
    $value = 1;
    $entry = Mockery::mock('EesyLDAP\Entry');
    $entry->shouldReceive('php2ldap')->passthru();
    $this->assertEquals(strval($value), $entry->php2ldap($attr_name, $value, true));
  }

  /**
   * @covers \EesyLDAP\Entry::php2ldap
   */
  public function testPhp2LdapNoLdapOneValueNull() {
    $attr_name = 'test';
    $entry = Mockery::mock('EesyLDAP\Entry');
    $entry->shouldReceive('php2ldap')->passthru();
    $this->assertNull($entry->php2ldap($attr_name, null, true));
  }

  /**
   * @covers \EesyLDAP\Entry::php2ldap
   */
  public function testPhp2LdapWithSchema() {
    $attr_name = 'test';
    $value = array('TRUE');
    $one_value = false;
    $attr = $this->createMock(\EesyLDAP\Schema\Attribute\BooleanAttribute::class);
    $attr->expects($this->once())
         ->method('php2ldap')
         ->with($this->equalTo($value), $this->equalTo($one_value))
         ->will($this->returnValue(true));
    $schema = Mockery::mock('EesyLDAP\Schema');
    $schema->shouldReceive('attribute')
      ->once()
      ->with($attr_name)
      ->andReturn($attr);

    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('schema'))
         ->will($this->returnValue($schema));

    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('php2ldap')->passthru();

    $this->assertTrue($entry->php2ldap($attr_name, $value, $one_value));
  }

  /**
   * @covers \EesyLDAP\Entry::php2ldap
   */
  public function testPhp2LdapWithSchemaUndefinedAttribute() {
    $attr_name = 'test';
    $value = array('TRUE');
    $schema = Mockery::mock('EesyLDAP\Schema');
    $schema->shouldReceive('attribute')
      ->once()
      ->with($attr_name)
      ->andReturn(false);

    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->atLeastOnce())
         ->method('__get')
         ->with($this->equalTo('schema'))
         ->will($this->returnValue($schema));

    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('error')
      ->once()
      ->with('Unkown attribute %s in schema', null, $attr_name)
      ->andThrow(LdapException::class);
    $entry->shouldReceive('php2ldap')->passthru();

    $this->expectException(LdapException::class);
    $entry->php2ldap($attr_name, $value);
  }

  /**
   * @covers \EesyLDAP\Entry::error
   */
  public function testError() {
    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry->shouldReceive('error')->passthru();
    $this->expectException(LdapException::class);
    $entry->error('test');
  }

  /**
   * @covers \EesyLDAP\Entry::error
   */
  public function testErrorWithArgs() {
    $error = 'test %s:%s';
    $arg1 = 'arg1';
    $arg2 = 'arg2';
    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry->shouldReceive('error')->passthru();
    $this->expectException(LdapException::class);
    $this->expectExceptionMessage(sprintf($error, $arg1, $arg2));
    $entry->error($error, $arg1, $arg2);
  }

  /**
   * @covers \EesyLDAP\Entry::error
   */
  public function testErrorWithLdap() {
    $error = 'test';
    $ldap = $this->createMock(\EesyLDAP\Ldap::class);
    $ldap->expects($this->once())
         ->method('error')
         ->with($this->equalTo($error))
         ->will($this->throwException(new LdapException));

    $entry = Mockery::mock('EesyLDAP\Entry')
      ->shouldAllowMockingProtectedMethods();
    $entry_reflection = new ReflectionClass($entry);
    $ldap_property = $entry_reflection->getProperty('ldap');
    $ldap_property->setAccessible(true);
    $ldap_property->setValue($entry, $ldap);

    $entry->shouldReceive('error')->passthru();

    $this->expectException(LdapException::class);
    $entry->error($error);
  }
}

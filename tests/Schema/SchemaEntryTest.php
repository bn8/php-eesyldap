<?php
declare(strict_types=1);
use \Mockery\Adapter\Phpunit\MockeryTestCase;

use EesyLDAP\InvalidPropertyException;
use EesyLDAP\Schema\Attribute;
use EesyLDAP\Schema\ObjectClass;
use EesyLDAP\Schema\SchemaEntry;


/**
 * @covers \EesyLDAP\Schema\SchemaEntry
 */
final class SchemaEntryTest extends MockeryTestCase {

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   */
  public function testConstruct() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'caseExactIA5Match',
      ),
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.26',
    );
    $entry = new SchemaEntry($data);
    $reflection = new ReflectionClass($entry);
    $data_property = $reflection->getProperty('data');
    $data_property->setAccessible(true);
    $this -> assertEquals($data, $data_property->getValue($entry));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   */
  public function testRawParseAttribute() {
    $value = "( 2.5.18.4 NAME 'modifiersName' DESC 'RFC4512: name of last modifier' EQUALITY ".
    "distinguishedNameMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.12 SINGLE-VALUE ".
    "NO-USER-MODIFICATION USAGE directoryOperation )";
    $expected = array (
      'oid' => '2.5.18.4',
      'name' => array(
        'modifiersName',
      ),
      'desc' => 'RFC4512: name of last modifier',
      'equality' => 'distinguishedNameMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.12',
      'single-value' => 1,
      'no-user-modification' => 1,
      'usage' => 'directoryOperation',
    );
    $this -> assertEquals($expected, SchemaEntry::_parse($value));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   */
  public function testRawParseAttributeWithMaxLength() {
    $value = "( 1.3.6.1.1.1.1.19 NAME 'ipHostNumber' DESC 'IP address' EQUALITY caseIgnoreIA5Match ".
    "SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{128} )";
    $expected = array (
      'oid' => '1.3.6.1.1.1.1.19',
      'name' => array(
        'ipHostNumber',
      ),
      'desc' => 'IP address',
      'equality' => 'caseIgnoreIA5Match',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.26{128}',
      'max_length' => 128,
    );
    $this -> assertEquals($expected, SchemaEntry::_parse($value));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   */
  public function testRawParseMatchingRule() {
    $value = "( 1.3.6.1.4.1.1466.109.114.1 NAME 'caseExactIA5Match' SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )";
    $expected = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'caseExactIA5Match',
      ),
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.26',
    );
    $this -> assertEquals($expected, SchemaEntry::_parse($value));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   */
  public function testRawParseMatchingRuleUse() {
    $value = "( 2.5.13.38 NAME 'certificateListExactMatch' APPLIES ( authorityRevocationList $ ".
    "certificateRevocationList $ deltaRevocationList ) )";
    $expected = array (
      'oid' => '2.5.13.38',
      'name' => array(
        'certificateListExactMatch',
      ),
      'applies' => array(
        'authorityRevocationList',
        'certificateRevocationList',
        'deltaRevocationList',
      ),
    );
    $this -> assertEquals($expected, SchemaEntry::_parse($value));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   */
  public function testRawParseObjectClass() {
    $value = "( 0.9.2342.19200300.100.4.5 NAME 'account' SUP top STRUCTURAL MUST userid MAY ".
    "( description $ seeAlso $ localityName $ organizationName $ organizationalUnitName $ host ) )";
    $expected = array (
      'oid' => '0.9.2342.19200300.100.4.5',
      'name' => array(
        'account',
      ),
      'sup' => array(
        'top',
      ),
      'structural' => 1,
      'must' => array(
        'userid',
      ),
      'may' => array(
        'description',
        'seeAlso',
        'localityName',
        'organizationName',
        'organizationalUnitName',
        'host',
      ),
    );
    $this -> assertEquals($expected, SchemaEntry::_parse($value));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   */
  public function testRawParseSyntax() {
    $value = "( 1.3.6.1.4.1.1466.115.121.1.8 DESC 'Certificate' X-BINARY-TRANSFER-REQUIRED 'TRUE' ".
        "X-NOT-HUMAN-READABLE 'TRUE' )";
    $expected = array (
      'oid' => '1.3.6.1.4.1.1466.115.121.1.8',
      'desc' => 'Certificate',
      'x-binary-transfer-required' => 'TRUE',
      'x-not-human-readable' => 'TRUE',
    );
    $this -> assertEquals($expected, SchemaEntry::_parse($value));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::parse
   */
  public function testParseEntry() {
    $value = "( 1.3.6.1.4.1.1466.115.121.1.8 DESC 'Certificate' X-BINARY-TRANSFER-REQUIRED 'TRUE' ".
        "X-NOT-HUMAN-READABLE 'TRUE' )";
    $entry = SchemaEntry::parse($value);
    $this -> assertInstanceOf(SchemaEntry::class, $entry);
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   * @covers \EesyLDAP\Schema\Attribute::__get
   */
  public function testGetProperties() {
    $data = array (
      'oid' => '2.5.4.46',
      'sup' => 'top',
      'desc' => 'RFC2256: DN qualifier',
      'equality' => 'caseIgnoreMatch',
      'ordering' => 'caseIgnoreOrderingMatch',
      'substr' => 'caseIgnoreSubstringsMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.44',
      'single-value' => 1,
      'no-user-modification' => 1,
      'usage' => 'directoryOperation',
    );
    $entry = new Attribute($data);
    foreach($data as $key => $value)
      $this -> assertEquals($value, $entry->__get($key));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   * @covers \EesyLDAP\Schema\Attribute::__get
   */
  public function testGetPropertyAlias() {
    $data = array (
      'oid' => '2.5.4.46',
      'sup' => 'top',
      'desc' => 'RFC2256: DN qualifier',
      'equality' => 'caseIgnoreMatch',
      'ordering' => 'caseIgnoreOrderingMatch',
      'substr' => 'caseIgnoreSubstringsMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.44',
      'single-value' => 1,
      'no-user-modification' => 1,
      'usage' => 'directoryOperation',
    );
    $entry = new Attribute($data);
    foreach($entry->property_aliases as $alias => $key)
      $this -> assertEquals($data[$key], $entry->__get($alias));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testGetInvalidProperty() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.115.121.1.8',
      'desc' => 'Certificate',
      'x-binary-transfer-required' => 'TRUE',
      'x-not-human-readable' => 'TRUE',
    );
    $entry = new SchemaEntry($data);
    $this->expectException(InvalidPropertyException::class);
    $entry->__get('undefined');
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testGetName() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.115.121.1.8',
      'desc' => 'Certificate',
      'x-binary-transfer-required' => 'TRUE',
      'x-not-human-readable' => 'TRUE',
    );
    $entry = new SchemaEntry($data);
    $this->assertEquals($data['oid'], $entry->__get('name'));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testGetNames() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.115.121.1.8',
      'name' => array('test'),
    );
    $entry = new SchemaEntry($data);
    $this->assertEquals($data['name'], $entry->__get('names'));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   * @covers \EesyLDAP\Schema\Attribute::__get
   */
  public function testGetPropertyDefault() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.115.121.1.8',
      'name' => array('test'),
    );
    $entry = new Attribute($data);
    $this->assertEquals(false, $entry->__get('collective'));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   * @covers \EesyLDAP\Schema\ObjectClass::__get
   */
  public function testGetPropertyCastAsArray() {
    $data = array (
      'may' => 'test',
    );
    $entry = new ObjectClass($data);
    $this->assertEquals(array($data['may']), $entry->__get('may'));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::__toString
   */
  public function testToString() {
    $data = array (
      'name' => array('test'),
    );
    $entry = new SchemaEntry($data);
    $this->assertEquals('EesyLDAP\Schema\SchemaEntry<test>', $entry->__toString());
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::is_me
   */
  public function testIsMeByOID() {
    $data = array (
      'oid' => '1.2.3.4',
    );
    $entry = new SchemaEntry($data);
    $this->assertTrue($entry->is_me($data['oid']));
    $this->assertFalse($entry->is_me('2.3.4'));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::is_me
   */
  public function testIsMeByNames() {
    $data = array (
      'name' => array('test'),
    );
    $entry = new SchemaEntry($data);
    $this->assertTrue($entry->is_me($data['name'][0]));
    $this->assertFalse($entry->is_me('badName'));
  }

  /**
   * @covers \EesyLDAP\Schema\SchemaEntry::is_me
   */
  public function testIsMeBadID() {
    $entry = new SchemaEntry(array());
    // @phpstan-ignore-next-line
    $this->assertFalse($entry->is_me(array()));
  }

}

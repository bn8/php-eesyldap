<?php
declare(strict_types=1);
use \Mockery\Adapter\Phpunit\MockeryTestCase;

use EesyLDAP\Schema\Attribute\BooleanAttribute;


/**
 * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute
 */
final class BooleanAttributeTest extends MockeryTestCase {

  /**
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::__construct
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpSingleValue() {
    $data = array (
      'oid' => '1.3.6.1.4.1.10650.3.987512.2.1.0',
      'name' => array(
        'slEnabled',
      ),
      'equality' => 'booleanMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.7',
      'single-value' => 1,
    );
    $attr = new BooleanAttribute($data);
    $this -> assertTrue($attr->ldap2php(array('TRUE')));
    $this -> assertFalse($attr->ldap2php(array('FALSE')));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::__construct
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpBadValue() {
    $data = array (
      'oid' => '1.3.6.1.4.1.10650.3.987512.2.1.0',
      'name' => array(
        'slEnabled',
      ),
      'equality' => 'booleanMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.7',
      'single-value' => 1,
    );
    $attr = new BooleanAttribute($data);
    $this -> assertNull($attr->ldap2php(array('???')));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::__construct
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpMultiple() {
    $data = array (
      'oid' => '1.3.6.1.4.1.10650.3.987512.2.1.0',
      'name' => array(
        'slEnabled',
      ),
      'equality' => 'booleanMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.7',
    );
    $attr = new BooleanAttribute($data);
    $this -> assertEquals(array(true, false), $attr->ldap2php(array('TRUE', 'FALSE')));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::__construct
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpNoValueSingle() {
    $data = array (
      'oid' => '1.3.6.1.4.1.10650.3.987512.2.1.0',
      'name' => array(
        'slEnabled',
      ),
      'equality' => 'booleanMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.7',
      'single-value' => 1,
    );
    $attr = new BooleanAttribute($data);
    $this -> assertEquals(null, $attr->ldap2php(array()));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::__construct
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\Attribute::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpNoValueMultiple() {
    $data = array (
      'oid' => '1.3.6.1.4.1.10650.3.987512.2.1.0',
      'name' => array(
        'slEnabled',
      ),
      'equality' => 'booleanMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.7',
    );
    $attr = new BooleanAttribute($data);
    $this -> assertEquals(array(), $attr->ldap2php(array()));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::__construct
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::php2ldap
   * @covers \EesyLDAP\Schema\Attribute::php2ldap
   * @covers \EesyLDAP\Schema\Attribute::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testPhp2LdapNotArray() {
    $data = array (
      'oid' => '1.3.6.1.4.1.10650.3.987512.2.1.0',
      'name' => array(
        'slEnabled',
      ),
      'equality' => 'booleanMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.7',
    );
    $attr = new BooleanAttribute($data);
    $this -> assertEquals(array('TRUE'), $attr->php2ldap(true));
    $this -> assertEquals(array('FALSE'), $attr->php2ldap(false));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::php2ldap
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testPhp2LdapOneValue() {
    $data = array (
      'oid' => '1.3.6.1.4.1.10650.3.987512.2.1.0',
      'name' => array(
        'slEnabled',
      ),
      'equality' => 'booleanMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.7',
    );
    $attr = new BooleanAttribute($data);
    $this -> assertEquals('TRUE', $attr->php2ldap(true, true));
    $this -> assertEquals('FALSE', $attr->php2ldap(false, true));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute\BooleanAttribute::php2ldap
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testPhp2LdapNull() {
    $data = array (
      'oid' => '1.3.6.1.4.1.10650.3.987512.2.1.0',
      'name' => array(
        'slEnabled',
      ),
      'equality' => 'booleanMatch',
      'syntax' => '1.3.6.1.4.1.1466.115.121.1.7',
    );
    $attr = new BooleanAttribute($data);
    $this -> assertEquals(array(), $attr->php2ldap(null));
    $this -> assertNull($attr->php2ldap(null, true));
  }

}

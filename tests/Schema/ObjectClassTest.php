<?php
declare(strict_types=1);
use \Mockery\Adapter\Phpunit\MockeryTestCase;

use EesyLDAP\Schema\ObjectClass;


/**
 * @covers \EesyLDAP\Schema\ObjectClass
 */
final class ObjectClassTest extends MockeryTestCase {

  /**
   * @covers \EesyLDAP\Schema\ObjectClass::__construct
   * @covers \EesyLDAP\Schema\ObjectClass::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testGetTypeStructural() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
      'structural' => 1,
    );
    $entry = new ObjectClass($data);
    $this -> assertEquals('structural', $entry->__get('type'));
  }

  /**
   * @covers \EesyLDAP\Schema\ObjectClass::__construct
   * @covers \EesyLDAP\Schema\ObjectClass::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testGetTypeAbstract() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
      'abstract' => 1,
    );
    $entry = new ObjectClass($data);
    $this -> assertEquals('abstract', $entry->__get('type'));
  }

  /**
   * @covers \EesyLDAP\Schema\ObjectClass::__construct
   * @covers \EesyLDAP\Schema\ObjectClass::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testGetTypeAuxilary() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
      'auxiliary' => 1,
    );
    $entry = new ObjectClass($data);
    $this -> assertEquals('auxiliary', $entry->__get('type'));
  }

  /**
   * @covers \EesyLDAP\Schema\ObjectClass::__construct
   * @covers \EesyLDAP\Schema\ObjectClass::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testGetTypeInvalid() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
    );
    $entry = new ObjectClass($data);
    $this -> assertNull($entry->__get('type'));
  }

  /**
   * @covers \EesyLDAP\Schema\ObjectClass::__construct
   * @covers \EesyLDAP\Schema\ObjectClass::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testHasAttribute() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
      'must' => array('uid'),
      'may' => array('cn'),
    );
    $entry = new ObjectClass($data);
    $this -> assertTrue($entry->has_attribute('uid'));
    $this -> assertTrue($entry->has_attribute('cn'));
    $this -> assertFalse($entry->has_attribute('o'));
  }

}

<?php
declare(strict_types=1);
use \Mockery\Adapter\Phpunit\MockeryTestCase;

use EesyLDAP\Schema\Attribute;
use EesyLDAP\Schema\Attribute\BooleanAttribute;


/**
 * @covers \EesyLDAP\Schema\Attribute
 */
final class AttributeTest extends MockeryTestCase {

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute::__get
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testGetMultiple() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
      'single-value' => 1,
    );
    $attr = new Attribute($data);
    $this -> assertFalse($attr->__get('multiple'));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpSingleValue() {
    $values = array('test');
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
      'single-value' => 1,
    );
    $attr = new Attribute($data);
    $this -> assertEquals($values[0], $attr->ldap2php($values));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpMultiple() {
    $values = array('test');
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
    );
    $attr = new Attribute($data);
    $this -> assertEquals($values, $attr->ldap2php($values));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpNoValueSingle() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
      'single-value' => 1,
    );
    $attr = new Attribute($data);
    $this -> assertEquals(null, $attr->ldap2php(array()));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testLdap2PhpNoValueMultiple() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
    );
    $attr = new Attribute($data);
    $this -> assertEquals(array(), $attr->ldap2php(array()));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testPhp2LdapNotArray() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
    );
    $attr = new Attribute($data);
    $this -> assertEquals(array('test'), $attr->php2ldap('test'));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testPhp2LdapOneValue() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
    );
    $attr = new Attribute($data);
    $this -> assertEquals('1', $attr->php2ldap(1, true));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::__construct
   * @covers \EesyLDAP\Schema\Attribute::ldap2php
   * @covers \EesyLDAP\Schema\SchemaEntry::__get
   */
  public function testPhp2LdapNull() {
    $data = array (
      'oid' => '1.3.6.1.4.1.1466.109.114.1',
      'name' => array(
        'test',
      ),
    );
    $attr = new Attribute($data);
    $this -> assertEquals(array(), $attr->php2ldap(null));
    $this -> assertNull($attr->php2ldap(null, true));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::parse
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   * @covers \EesyLDAP\Schema\SchemaEntry::__construct
   * @covers \EesyLDAP\Schema\SchemaEntry::_tokenize
   */
  public function testParse() {
    $value = "( 2.5.18.4 NAME 'modifiersName' DESC 'RFC4512: name of last modifier' EQUALITY ".
    "distinguishedNameMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.12 SINGLE-VALUE ".
    "NO-USER-MODIFICATION USAGE directoryOperation )";
    $this -> assertInstanceOf(Attribute::class, Attribute::parse($value));
  }

  /**
   * @covers \EesyLDAP\Schema\Attribute::parse
   * @covers \EesyLDAP\Schema\SchemaEntry::_parse
   * @covers \EesyLDAP\Schema\SchemaEntry::__construct
   * @covers \EesyLDAP\Schema\SchemaEntry::_tokenize
   */
  public function testParseBoolean() {
    $value = "( 1.3.6.1.4.1.10650.3.987512.2.1.0 NAME 'slEnabled' DESC 'SimpleLDAP enable state' ".
    "EQUALITY booleanMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )";
    $this -> assertInstanceOf(BooleanAttribute::class, Attribute::parse($value));
  }

}

<?php
declare(strict_types=1);
use \Mockery\Adapter\Phpunit\MockeryTestCase;
use PHPUnit\Extension\FunctionMocker;
use EesyLDAP\Link;


/**
 * @covers \EesyLDAP\Link
 */
final class LinkTest extends MockeryTestCase {

  /**
   * @var FunctionMocker
   */
  protected $func;

  public function setUp(): void {
    $this->func = FunctionMocker::start($this, 'EesyLDAP')
                  ->mockFunction('ldap_connect')
                  ->mockFunction('ldap_start_tls')
                  ->mockFunction('ldap_errno')
                  ->mockFunction('ldap_err2str')
                  ->mockFunction('ldap_bind')
                  ->mockFunction('ldap_set_option')
                  ->mockFunction('ldap_list')
                  ->mockFunction('ldap_read')
                  ->mockFunction('ldap_search')
                  ->mockFunction('ldap_get_entries')
                  ->mockFunction('ldap_close')
                  ->getMock();
  }

  /**
   * @covers \EesyLDAP\Link::connect
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testConnect() {
    $link = new Link();
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue(true));
    $link->connect('ldap://host', 389);
  }

  /**
   * @covers \EesyLDAP\Link::connect
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testConnectNoPort() {
    $link = new Link();
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
  }

  /**
   * @covers \EesyLDAP\Link::start_tls
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testStartTLS() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_start_tls')
               ->with($ldap_link)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue($link->start_tls());
  }

  /**
   * @covers \EesyLDAP\Link::start_tls
   */
  public function testStartTLSNoLink() {
    $link = new Link();
    $this->assertFalse($link->start_tls());
  }

  /**
   * @covers \EesyLDAP\Link::errno
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testErrNo() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $error = 186;
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_errno')
               ->with($ldap_link)
               ->will($this->returnValue($error));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertEquals($error, $link->errno());
  }

  /**
   * @covers \EesyLDAP\Link::errno
   */
  public function testErrNoNoLink() {
    $link = new Link();
    $this->assertFalse($link->errno());
  }

  /**
   * @covers \EesyLDAP\Link::err2str
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testErr2Str() {
    $link = new Link();
    $error = 186;
    $error_msg = 'oups';
    $this->func->expects($this->once())
               ->method('ldap_err2str')
               ->with($error)
               ->will($this->returnValue($error_msg));
    $this->assertEquals($error_msg, $link->err2str($error));
  }

  /**
   * @covers \EesyLDAP\Link::bind
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testBind() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $dn = 'cn=test';
    $password = 'secret';
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_bind')
               ->with($ldap_link, $dn, $password)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue($link->bind($dn, $password));
  }

  /**
   * @covers \EesyLDAP\Link::bind
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testBindNoParams() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_bind')
               ->with($ldap_link, null, null)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue($link->bind());
  }

  /**
   * @covers \EesyLDAP\Link::errno
   */
  public function testBindNoLink() {
    $link = new Link();
    $this->assertFalse($link->bind('cn=test', 'secret'));
  }

  /**
   * @covers \EesyLDAP\Link::set_option
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testSetOption() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $option = 10;
    $value = 'value';
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_set_option')
               ->with($ldap_link, $option, $value)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue($link->set_option($option, $value));
  }

  /**
   * @covers \EesyLDAP\Link::set_option
   */
  public function testSetOptionNoLink() {
    $link = new Link();
    $this->assertFalse($link->set_option(10, 'test'));
  }

  /**
   * @covers \EesyLDAP\Link::list
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testList() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $base = 'o=example';
    $filter = 'cn=test';
    $attributes = array('cn');
    $attributes_only=1;
    $sizelimit = 10;
    $timelimit = 30;
    $deref =  LDAP_DEREF_ALWAYS;
	  $controls = array();
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_list')
               ->with(
                 $ldap_link, $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit,
                 $deref, $controls)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue(
      $link->list(
        $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit, $deref, $controls
      )
    );
  }

  /**
   * @covers \EesyLDAP\Link::list
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testListWithMinimalParameters() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $base = 'o=example';
    $filter = 'cn=test';
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_list')
               ->with($ldap_link, $base, $filter, [], 0, -1, -1,  LDAP_DEREF_NEVER, [])
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue($link->list($base, $filter));
  }

  /**
   * @covers \EesyLDAP\Link::list
   */
  public function testListNoLink() {
    $link = new Link();
    $this->assertFalse($link->list('o=example', 'test=value'));
  }

  /**
   * @covers \EesyLDAP\Link::read
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testRead() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $base = 'o=example';
    $filter = 'cn=test';
    $attributes = array('cn');
    $attributes_only=1;
    $sizelimit = 10;
    $timelimit = 30;
    $deref =  LDAP_DEREF_ALWAYS;
	  $controls = array();
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_read')
               ->with(
                 $ldap_link, $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit,
                 $deref, $controls)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue(
      $link->read(
        $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit, $deref, $controls
      )
    );
  }

  /**
   * @covers \EesyLDAP\Link::read
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testReadWithMinimalParameters() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $base = 'o=example';
    $filter = 'cn=test';
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_read')
               ->with($ldap_link, $base, $filter, [], 0, -1, -1,  LDAP_DEREF_NEVER, [])
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue($link->read($base, $filter));
  }

  /**
   * @covers \EesyLDAP\Link::read
   */
  public function testReadNoLink() {
    $link = new Link();
    $this->assertFalse($link->read('o=example', 'test=value'));
  }

  /**
   * @covers \EesyLDAP\Link::search
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testSearch() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $base = 'o=example';
    $filter = 'cn=test';
    $attributes = array('cn');
    $attributes_only=1;
    $sizelimit = 10;
    $timelimit = 30;
    $deref =  LDAP_DEREF_ALWAYS;
	  $controls = array();
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_search')
               ->with(
                 $ldap_link, $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit,
                 $deref, $controls)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue(
      $link->search(
        $base, $filter, $attributes, $attributes_only, $sizelimit, $timelimit, $deref, $controls
      )
    );
  }

  /**
   * @covers \EesyLDAP\Link::search
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testSearchWithMinimalParameters() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $base = 'o=example';
    $filter = 'cn=test';
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_search')
               ->with($ldap_link, $base, $filter, [], 0, -1, -1,  LDAP_DEREF_NEVER, [])
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $this->assertTrue($link->search($base, $filter));
  }

  /**
   * @covers \EesyLDAP\Link::search
   */
  public function testSearchNoLink() {
    $link = new Link();
    $this->assertFalse($link->search('o=example', 'test=value'));
  }

  /**
   * @covers \EesyLDAP\Link::get_entries
   * @runInSeparateProcess
   * @preserveGlobalState disabled
   */
  public function testGetEntries() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $search_ref = 'search_ref';
    $expected_result = array('test');
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_get_entries')
               ->with($ldap_link, $search_ref)
               ->will($this->returnValue($expected_result));
    $this->assertTrue($link->connect('ldap://host'));
    // @phpstan-ignore-next-line
    $this->assertEquals($expected_result, $link->get_entries($search_ref));
  }

  /**
   * @covers \EesyLDAP\Link::get_entries
   */
  public function testGetEntriesNoLink() {
    $link = new Link();
    // @phpstan-ignore-next-line
    $this->assertFalse($link->get_entries('search_ref'));
  }

  /**
   * @covers \EesyLDAP\Link::close
   */
  public function testClose() {
    $link = new Link();
    $ldap_link = 'ldap_link';
    $search_ref = 'search_ref';
    $expected_result = array('test');
    $this->func->expects($this->once())
               ->method('ldap_connect')
               ->with('ldap://host', 389)
               ->will($this->returnValue($ldap_link));
    $this->func->expects($this->once())
               ->method('ldap_close')
               ->with($ldap_link)
               ->will($this->returnValue(true));
    $this->assertTrue($link->connect('ldap://host'));
    $link->close();
  }

  /**
   * @covers \EesyLDAP\Link::close
   */
  public function testCloseNoLink() {
    $link = new Link();
    $this->func->expects($this->never())
               ->method('ldap_close');
    $link->close();
  }
}

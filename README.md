# EesyLDAP (PHP)

PHP object oriented interface for searching and manipulating LDAP entries & filters.

__Notes:__

* work in progress. Start by implementing LDAP filter abstraction class.
* freely inspired by some other open-source projects and notably PEAR
[Net_LDAP2](https://pear.php.net/package/Net_LDAP2) (abandoned).

## Installation

```bash
composer require brenard/eesyldap
```

## Usage

### Manipulating LDAP filter strings

To create a new filter:
```php
// Match objects with uid equals 'admin'
// (uid=admin)
$filter = new \EesyLDAP\Filter('uid', 'equals', 'admin');

// Match objects with uid equals 'admin' and mail equals 'admin@example.com'
// (&(uid=admin)(mail=admin@example.com))
$filter = new \EesyLDAP\Filter(
  'and',
  new \EesyLDAP\Filter('uid', '=', 'admin'),
  'mail=admin@example.com'
);

// Match objects with uid different than admin
// (!(uid=admin))
$filter = new \EesyLDAP\Filter('not', 'uid=admin');

// Match objects with uid attribute present
// (uid=*)
$filter = new \EesyLDAP\Filter('uid', 'present');
```

By default, all pattern value are automatically escape. To control it, add a boolean as last Filter
constructor argument:

```php
// Disable automatic escaping
// (uid=admin-*)
$filter = new \EesyLDAP\Filter('uid', '=', 'admin-*', false);
```

You also could escape/unescape value manually:

```php
$escaped = \EesyLDAP\Filter::escape('Administrator (admin)');
$unescaped = \EesyLDAP\Filter::unescape('Administrator \28admin\29');
```

You also could parse an existing filter string with the `parse()` method:

```php
$filter = \EesyLDAP\Filter::parse('(&(uid=admin)(mail=admin@example.com))');
```

## License

[GNU Lesser General Public License v3 (or later)](https://www.gnu.org/licenses/lgpl-3.0.en.html).
